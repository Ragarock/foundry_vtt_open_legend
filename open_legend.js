import {open_legend} from "./module/config.js";
import open_legendItemSheet from "./module/items/item.js";
import open_legendActorSheet from "./module/actors/actor.js";

async function preload_handlebars_templates() {
	const paths = [
		"systems/open_legend/templates/parts/character-stats.html",
		"systems/open_legend/templates/parts/attribute-block.html",
	];

	return loadTemplates(paths);
}

Hooks.once("init", function() {
	console.log("open_legend | Initialising Open Legend RPG system");

	CONFIG.open_legend = open_legend;

	Items.unregisterSheet("core", ItemSheet);
	Items.registerSheet("open_legend", open_legendItemSheet, {makeDefault: true});

	Actors.unregisterSheet("core", ActorSheet);
	Actors.registerSheet("open_legend", open_legendActorSheet, {makeDefault: true})

	preload_handlebars_templates();
});
