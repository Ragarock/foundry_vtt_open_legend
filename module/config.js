export const open_legend = {};

open_legend.weapon_categories = {
	one_handed_melee: "open_legend_lang.weapon_categories.one_handed_melee",
	two_handed_melee: "open_legend_lang.weapon_categories.two_handed_melee",
	versatile_melee: "open_legend_lang.weapon_categories.versatile_melee",
	close_ranged: "open_legend_lang.weapon_categories.close_ranged",
	short_ranged: "open_legend_lang.weapon_categories.short_ranged",
	medium_ranged: "open_legend_lang.weapon_categories.medium_ranged",
	long_ranged: "open_legend_lang.weapon_categories.long_ranged",
	extreme_ranged: "open_legend_lang.weapon_categories.extreme_ranged"
}

open_legend.weapon_properties = {
	area: "open_legend_lang.weapon_properties.area",
	expendable: "open_legend_lang.weapon_properties.expendable",
	defensive: "open_legend_lang.weapon_properties.defensive",
	delayed_ready: "open_legend_lang.weapon_properties.delayed_ready",
	forceful: "open_legend_lang.weapon_properties.forceful",
	heavy: "open_legend_lang.weapon_properties.heavy",
	reach: "open_legend_lang.weapon_properties.reach",
	slow: "open_legend_lang.weapon_properties.slow",
	stationary: "open_legend_lang.weapon_properties.stationary",
	swift: "open_legend_lang.weapon_properties.swift"
}

open_legend.banes = {}
open_legend.boons = {}
