export default class open_legendItemSheet extends ItemSheet {
	static get defaultOptions() {
		return mergeObject(super.defaultOptions, {
			width: 500,
			height: 600,
			classes: ["open_legend", "sheet", "item"]
		});
	}

	get template() {
		return `systems/open_legend/templates/items/${this.item.data.type}.html`;
	}

	getData() {
		const data = super.getData();

		data.config = CONFIG.open_legend;

		return data;
	}
}
