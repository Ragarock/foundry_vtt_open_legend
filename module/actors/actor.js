export default class open_legendActorSheet extends ActorSheet {
	static get defaultOptions() {
		return mergeObject(super.defaultOptions, {
			width: 500,
			height: 600,
			classes: ["open_legend", "sheet", "actor"]
		});
	}

	get template() {
		return `systems/open_legend/templates/actors/${this.actor.data.type}.html`;
	}

	getData() {
		const data = super.getData();

		data.config = CONFIG.open_legend;
		data.actions = data.items.filter(function(item) {return item.type == "action"});
		data.weapons = data.items.filter(function(item) {return item.type == "weapon"});

		return data;
	}

	activeListeners(html) {
		html.find(".item-create").click(this._onItemCreate.bind(this));

		super.activateListeners(html);
	}

	_onItemCreate(event) {
		event.preventDefault();
		let element = event.currentTarget;

		let itemData = {
			name: game.i18n.localize("open_legend_lang.new_item"),
			type: element.dataset.type
		};

		return this.actor.createOwnItem(itemData);
	}
}
